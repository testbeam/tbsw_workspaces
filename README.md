Collection of tbsw workspaces 

This repository collects real world tbsw workspaces from different user groups. The workspaces contain scripts for 
processing data, geometry description files and data base files describing run and experiment conditions during
the test beam period. Raw files and processed data should not be uploaded. 

The main motivations are: 

- Versioning for workspaces to make analysis results more reproducible
- Easy exchange of workspaces between group members at different sites 
- Real world examples for how to set up a workspace and organize data analysis    


The name of the workspace folder should contain a combination of the DUT name and the data taking period. Multiple
workspaces for the same DUT should be grouped in a seperate folder. Workspaces should contain a short README indicating 
the tbsw version (commit ID) used for data analysis. 

Cheers, 
Benjamin Schwenker 
