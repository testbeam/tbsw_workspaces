#!/usr/bin/env python
# coding: utf8
"""
Script for dumping interpreted Hits data for subdetector .h5 file to .csv files.

Usage: python csv_dumper.py --folder=angular_scan_TJMonopix/run53

Author: Benjamin Schwenker <benjamin.schwenker@phys.uni-goettingen.de>  
"""

import tables
import os

def dump_m26(rawfilename):
    
    # Create output file name
    output_filename=os.path.splitext(rawfilename)[0] + '.csv' 

    # Open a file in "r"ead mode
    h5file = tables.open_file(rawfilename, mode = "r")
    
    # Get the HDF5 table Hits
    Hits = h5file.root.Hits
    
    # Open a file to dump csv data
    outputfile = open(output_filename,"w") 
    
    for hit in Hits: 
        outputfile.write( "{:d},{:d},{:d},{:d},{:d}\n".format(hit['event_number'],hit['plane'],hit['column'],hit['row'],1) )
     
    outputfile.close()
    h5file.close()

def dump_fei(rawfilename):
    
    # Create output file name
    output_filename=os.path.splitext(rawfilename)[0] + '.csv' 

    # Open a file in "r"ead mode
    h5file = tables.open_file(rawfilename, mode = "r")
    
    # Get the HDF5 table Hits
    Hits = h5file.root.Hits
    
    # Open a file to dump csv data
    outputfile = open(output_filename,"w") 
    
    for hit in Hits: 
        outputfile.write( "{:d},{:d},{:d},{:d},{:d}\n".format(hit['event_number'],21,hit['column'],hit['row'],hit['tot']) )
     
    outputfile.close()
    h5file.close()

def dump_tj(rawfilename, event_offset=0):
    
    # Create output file name
    output_filename=os.path.splitext(rawfilename)[0] + '.csv' 

    # Open a file in "r"ead mode
    h5file = tables.open_file(rawfilename, mode = "r")
    
    # Get the HDF5 table Hits
    Hits = h5file.root.Hits
    
    # Open a file to dump csv data
    outputfile = open(output_filename,"w") 
    
    for hit in Hits: 
        outputfile.write( "{:d},{:d},{:d},{:d},{:d}\n".format(hit['event_number']-event_offset,22,hit['column'],hit['row'],hit['charge']) )
     
    outputfile.close()
    h5file.close()

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Dump interpreted data to csv format")
    parser.add_argument('--folder', dest='folder', default='', type=str, help='Full path to folder with interpreted data')
    parser.add_argument('--tjoffset', dest='tjoffset', default=0, type=int, help='Event number offset for tjmonopix')
    args = parser.parse_args()
    
    import glob
    
    rawfilename = glob.glob( args.folder + '/pymosa_*_pyBar_interpreted.h5' )[0] 
    print('Create csv data for file ' + rawfilename)
    dump_m26(rawfilename)

     
    rawfilename = glob.glob( args.folder + '/*_module_0_anemone_scan_interpreted.h5' )[0]  
    print('Create csv data for file ' + rawfilename)
    dump_fei(rawfilename)

    rawfilename = glob.glob( args.folder + '/tjmonopix__interpreted.h5' )[0]
    print('Create csv data for file ' + rawfilename)
    dump_tj(rawfilename, args.tjoffset)

    






