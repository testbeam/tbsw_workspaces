#!/usr/bin/env python
# coding: utf8
"""
Script for submitting all jobs for preparing csv dumps for tjmonopix testbeam 
March 2020 at Desy. 

Usage: python submit_csv_dumper_jobs.py

Author: Benjamin Schwenker <benjamin.schwenker@phys.uni-goettingen.de>  
"""

import multiprocessing
import subprocess

tasks = [ 
          "python csv_dumper.py --folder=angular_scan_TJMonopix/run46  --tjoffset=1",
          "python csv_dumper.py --folder=angular_scan_TJMonopix/run52  --tjoffset=0",
          #"python csv_dumper.py --folder=angular_scan_TJMonopix/run53  --tjoffset=0",
          #"python csv_dumper.py --folder=angular_scan_TJMonopix/run54  --tjoffset=0",
          "python csv_dumper.py --folder=angular_scan_TJMonopix/run55  --tjoffset=1",
          #"python csv_dumper.py --folder=angular_scan_TJMonopix/run67  --tjoffset=1", 
          "python csv_dumper.py --folder=angular_scan_TJMonopix/run69  --tjoffset=0", 
          #"python csv_dumper.py --folder=angular_scan_TJMonopix/run70  --tjoffset=1",
          "python csv_dumper.py --folder=angular_scan_TJMonopix/run71  --tjoffset=1",
          #"python csv_dumper.py --folder=angular_scan_TJMonopix/run72  --tjoffset=1",
          "python csv_dumper.py --folder=angular_scan_TJMonopix/run75  --tjoffset=0", 
          #"python csv_dumper.py --folder=angular_scan_TJMonopix/run76  --tjoffset=1",
          "python csv_dumper.py --folder=angular_scan_TJMonopix/run79  --tjoffset=1", 
          #"python csv_dumper.py --folder=angular_scan_TJMonopix/run80  --tjoffset=1",
          "python csv_dumper.py --folder=angular_scan_TJMonopix/run82  --tjoffset=1",
        ]


def work(action):
    """
    Multiprocessing work
    
    Parameters
    ----------
    action : str
            command to be executed in shell to do work
    """
    print ('[INFO] ' + action + ' started ...')        
    retval=subprocess.call(action, shell=True)
    if retval < 0:
      raise RuntimeError("Process %s killed by signal %d "%(xmlfile, -retval))
    elif retval > 0:
      raise RuntimeError("Process %s failed with exit code %d "%(xmlfile, retval))
    print ('[INFO] ' + action + ' is done')          

    
if __name__ == "__main__":  
    print("There are %d CPUs on this machine" % multiprocessing.cpu_count())
    number_processes = 3
    pool = multiprocessing.Pool(number_processes)
    results = pool.map_async(work, tasks)
    pool.close()
    pool.join()

 
