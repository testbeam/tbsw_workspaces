#!/usr/bin/env python
# coding: utf8
"""
Script for processing tjmonopix testbeam March 2020 at Desy. 

This script shows the calibration and reconstruction process for testbeam data 
starting from interpreted .csv files. 

Usage: 

python tj-reco.py --folder=angular_scan_TJMonopix/run53 --gearfile=gear_tjmono_60deg.xml --runno=53


Author: Benjamin Schwenker <benjamin.schwenker@phys.uni-goettingen.de>  
"""

import tbsw 
import os

# Max number of events for long calibration task (-1 for all)
maxRecordNrLong  = 1500000
# Max number of events for short calibration task (-1 for all)
maxRecordNrShort = 600000
# Max number of events for reco path (-1 for all)
maxRecordNrLongReco = -1
# Skip first events during calibration. Does not work anyway. 
skipEvt = 0


def add_unpackers(path):
  """
  Adds unpackers to the path
  """  
  
  m26unpacker = tbsw.Processor(name="M26Unpacker",proctype="HitsFilterProcessor")   
  m26unpacker.param("InputCollectionName", "zsdata")
  m26unpacker.param("OutputCollectionName","zsdata_m26")
  m26unpacker.param("FilterIDs","1 2 3 4 5 6")
  path.add_processor(m26unpacker)
  
  fei4unpacker = tbsw.Processor(name="FEI4Unpacker",proctype="HitsFilterProcessor")
  fei4unpacker.param("InputCollectionName","zsdata")
  fei4unpacker.param("OutputCollectionName", "zsdata_fei4")
  fei4unpacker.param("FilterIDs","21")
  path.add_processor(fei4unpacker)   
  
  tjunpacker = tbsw.Processor(name="TJUnpacker",proctype="HitsFilterProcessor")
  tjunpacker.param('InputCollectionName', "zsdata")
  tjunpacker.param('OutputCollectionName','zsdata_tj')
  tjunpacker.param("FilterIDs","22")
  path.add_processor(tjunpacker)
  
 
  return path


def add_clusterizers(path):
  """
  Adds clusterizers to the path
  """  
    
  m26clust = tbsw.Processor(name="M26Clusterizer",proctype="PixelClusterizer")   
  m26clust.param("NoiseDBFileName","localDB/NoiseDB-M26.root")
  m26clust.param("SparseDataCollectionName","zsdata_m26")
  m26clust.param("ClusterCollectionName","zscluster_m26")
  m26clust.param("SparseClusterCut",0)
  m26clust.param("SparseSeedCut", 0)
  m26clust.param("SparseZSCut", 0)   
  path.add_processor(m26clust)  

  fei4clust = tbsw.Processor(name="FEI4Clusterizer",proctype="PixelClusterizer")   
  fei4clust.param("NoiseDBFileName","localDB/NoiseDB-FEI4.root")
  fei4clust.param("SparseDataCollectionName","zsdata_fei4")
  fei4clust.param("ClusterCollectionName","zscluster_fei4")
  fei4clust.param("SparseClusterCut",0)
  fei4clust.param("SparseSeedCut", 0)
  fei4clust.param("SparseZSCut", 0)   
  path.add_processor(fei4clust)  

  tjclust = tbsw.Processor(name="TJClusterizer",proctype="PixelClusterizer")   
  tjclust.param("NoiseDBFileName","localDB/NoiseDB-TJ.root")
  tjclust.param("SparseDataCollectionName","zsdata_tj")
  tjclust.param("ClusterCollectionName","zscluster_tj")
  tjclust.param("SparseClusterCut", -1)
  tjclust.param("SparseSeedCut", -1)
  tjclust.param("SparseZSCut", -1)   
  path.add_processor(tjclust)  
  

  return path

def add_hitmakers(path):
  """
  Adds center of gravity hitmakers to the path
  """  

  m26hitmaker = tbsw.Processor(name="M26CogHitMaker",proctype="CogHitMaker")
  m26hitmaker.param("ClusterCollection","zscluster_m26")
  m26hitmaker.param("HitCollectionName","hit_m26")
  m26hitmaker.param("SigmaUCorrections", "0.698 0.31 0.315")  
  m26hitmaker.param("SigmaVCorrections", "0.698 0.31 0.315")
  path.add_processor(m26hitmaker)

  fei4hitmaker = tbsw.Processor(name="FEI4CogHitMaker",proctype="CogHitMaker")
  fei4hitmaker.param("ClusterCollection","zscluster_fei4")
  fei4hitmaker.param("HitCollectionName","hit_fei4")
  fei4hitmaker.param("SigmaUCorrections", "1.0 0.5 0.3")  
  fei4hitmaker.param("SigmaVCorrections", "1.0 0.5 0.3")
  path.add_processor(fei4hitmaker)

  tjhitmaker = tbsw.Processor(name="TJCogHitMaker",proctype="CogHitMaker")
  tjhitmaker.param("ClusterCollection","zscluster_tj")
  tjhitmaker.param("HitCollectionName","hit_tj")
  tjhitmaker.param("SigmaUCorrections", "0.8 0.3 0.3")  
  tjhitmaker.param("SigmaVCorrections", "0.8 0.3 0.3")
  path.add_processor(tjhitmaker)
  

  
  return path

def add_hitmakersDB(path):
  """
  Add cluster shape hitmakers to the path (requiring clusterDBs)
  """  
  
  m26goehitmaker = tbsw.Processor(name="M26GoeHitMaker",proctype="GoeHitMaker")   
  m26goehitmaker.param("ClusterCollection","zscluster_m26")
  m26goehitmaker.param("HitCollectionName","hit_m26")
  m26goehitmaker.param("ClusterDBFileName","localDB/clusterDB-M26.root")
  path.add_processor(m26goehitmaker)  
    
  fei4goehitmaker = tbsw.Processor(name="FEI4GoeHitMaker",proctype="GoeHitMaker")   
  fei4goehitmaker.param("ClusterCollection","zscluster_fei4")
  fei4goehitmaker.param("HitCollectionName","hit_fei4")
  fei4goehitmaker.param("ClusterDBFileName","localDB/clusterDB-FEI4.root")
  path.add_processor(fei4goehitmaker) 
  
  tjgoehitmaker = tbsw.Processor(name="TJGoeHitMaker",proctype="GoeHitMaker")   
  tjgoehitmaker.param("ClusterCollection","zscluster_tj")
  tjgoehitmaker.param("HitCollectionName","hit_tj")
  tjgoehitmaker.param("ClusterDBFileName","localDB/clusterDB-TJ.root")
  tjgoehitmaker.param("UseCenterOfGravityFallback","true")
  tjgoehitmaker.param("SigmaUCorrections", "0.8 0.3 0.3")  
  tjgoehitmaker.param("SigmaVCorrections", "0.8 0.3 0.3")
  path.add_processor(tjgoehitmaker)   
  


  return path

def add_clustercalibrators(path):
  """
  Add cluster calibration processors to create clusterDB's
  """
  
  m26clustdb = tbsw.Processor(name="M26ClusterCalibrator",proctype="GoeClusterCalibrator")   
  m26clustdb.param("ClusterDBFileName","localDB/clusterDB-M26.root")  
  m26clustdb.param("MinClusters","200")
  m26clustdb.param("SelectPlanes","1 2 4 5")
  path.add_processor(m26clustdb)  
    
  tjclustdb = tbsw.Processor(name="TJClusterCalibrator",proctype="GoeClusterCalibrator")   
  tjclustdb.param("ClusterDBFileName","localDB/clusterDB-TJ.root")  
  tjclustdb.param("MinClusters","200")
  tjclustdb.param("MaxEtaBins","7")
  tjclustdb.param("SelectPlanes","3")
  path.add_processor(tjclustdb)  
    
  fei4clustdb = tbsw.Processor(name="FEI4ClusterCalibrator",proctype="GoeClusterCalibrator")   
  fei4clustdb.param("ClusterDBFileName","localDB/clusterDB-FEI4.root")  
  fei4clustdb.param("MinClusters","200")
  fei4clustdb.param("SelectPlanes","7")
  path.add_processor(fei4clustdb)  
  
  return path

def create_calibration_path(Env, rawfile, gearfile, energy, useClusterDB, runno):
  """
  Returns a list of tbsw path objects needed to calibrate the tracking telescope
  """
   
  
  
  # Calibrations are organized in a sequence of calibration paths. 
  # The calibration paths are collected in a list for later execution
  calpaths = []
  
  # Create path for detector level masking of hot channels 
  mask_path = Env.create_path('mask_path')
  mask_path.set_globals(params={'GearXMLFile': gearfile , 'MaxRecordNumber' : maxRecordNrLong, 'SkipNEvents' : skipEvt })
  
  rawinput = tbsw.Processor(name="RawInputProcessor",proctype="AsciiInputProcessor")
  rawinput.param("RawHitCollectionName", "zsdata")
  rawinput.param("RunNumber", runno)
  rawinput.param('FileNames', rawfile)
  mask_path.add_processor(rawinput)
  
  geo = tbsw.Processor(name="Geo",proctype="Geometry")
  geo.param("AlignmentDBFilePath", "localDB/alignmentDB.root")
  geo.param("ApplyAlignment", "true")
  geo.param("OverrideAlignment", "true")
  mask_path.add_processor(geo)
  
  mask_path = add_unpackers(mask_path)
   
  m26hotpixelkiller = tbsw.Processor(name="M26HotPixelKiller",proctype="HotPixelKiller")
  m26hotpixelkiller.param("InputCollectionName", "zsdata_m26")
  m26hotpixelkiller.param("MaxOccupancy", 0.001)
  m26hotpixelkiller.param("NoiseDBFileName", "localDB/NoiseDB-M26.root")
  m26hotpixelkiller.param("OfflineZSThreshold", 0)
  mask_path.add_processor(m26hotpixelkiller)
   
  fei4hotpixelkiller = tbsw.Processor(name="FEI4HotPixelKiller", proctype="HotPixelKiller")
  fei4hotpixelkiller.param("InputCollectionName", "zsdata_fei4")
  fei4hotpixelkiller.param("MaxOccupancy", 0.001)
  fei4hotpixelkiller.param("NoiseDBFileName", "localDB/NoiseDB-FEI4.root")
  fei4hotpixelkiller.param("OfflineZSThreshold", 0)
  mask_path.add_processor(fei4hotpixelkiller)
   
  tjhotpixelkiller = tbsw.Processor(name="TJHotPixelKiller", proctype="HotPixelKiller")
  tjhotpixelkiller.param("InputCollectionName", "zsdata_tj")
  tjhotpixelkiller.param("MaxOccupancy", 0.001)
  tjhotpixelkiller.param("NoiseDBFileName", "localDB/NoiseDB-TJ.root")
  tjhotpixelkiller.param("OfflineZSThreshold", 0)
  mask_path.add_processor(tjhotpixelkiller)  
  
  # Add path for masking
  calpaths.append(mask_path)  

  # Create path for detector level creation of clusters
  clusterizer_path = Env.create_path('clusterizer_path')
  clusterizer_path.set_globals(params={'GearXMLFile': gearfile , 'MaxRecordNumber' :  maxRecordNrLong, 'SkipNEvents' : skipEvt})
  
  clusterizer_path.add_processor(rawinput)  
  clusterizer_path.add_processor(geo)
  clusterizer_path = add_unpackers(clusterizer_path) 
  clusterizer_path = add_clusterizers(clusterizer_path)    
   
  lciooutput = tbsw.Processor(name="LCIOOutput",proctype="LCIOOutputProcessor")
  lciooutput.param("LCIOOutputFile","tmp.slcio")
  lciooutput.param("LCIOWriteMode","WRITE_NEW")
  clusterizer_path.add_processor(lciooutput)  
   
  # Finished with path for clusterizers
  calpaths.append(clusterizer_path)   
  
  # Frome here we do not need to skip, because the first events are already skipped in 'tmp.slcio'

  # Create path for pre alignmnet and dqm based on hits
  correlator_path = Env.create_path('correlator_path')
  correlator_path.set_globals(params={'GearXMLFile': gearfile , 'MaxRecordNumber' : maxRecordNrShort, 'LCIOInputFiles': "tmp.slcio" })
  correlator_path.add_processor(geo)
  correlator_path = add_hitmakers(correlator_path) 
  
  hitdqm = tbsw.Processor(name="RawDQM",proctype="RawHitDQM")
  hitdqm.param("InputHitCollectionNameVec","hit_m26 hit_fei4 hit_tj")  
  hitdqm.param("RootFileName","RawDQM.root")
  correlator_path.add_processor(hitdqm)  
   
  correlator = tbsw.Processor(name="TelCorrelator", proctype="Correlator")
  correlator.param("InputHitCollectionNameVec","hit_m26 hit_fei4 hit_tj")
  correlator.param("OutputRootFileName","XCorrelator.root")
  correlator.param("ReferencePlane","0")
  correlator.param("ParticleCharge","-1")
  correlator.param("ParticleMass","0.000511")
  correlator.param("ParticleMomentum", energy)
  correlator_path.add_processor(correlator)  
  
  # Finished with path for hit based pre alignment
  calpaths.append(correlator_path)  
  
  # Create path for pre alignment with loose cut track sample 
  prealigner_path = Env.create_path('prealigner_path')
  prealigner_path.set_globals(params={'GearXMLFile': gearfile , 'MaxRecordNumber' : maxRecordNrShort, 'LCIOInputFiles': "tmp.slcio" })
  prealigner_path.add_processor(geo)
  prealigner_path = add_hitmakers(prealigner_path)
   
  trackfinder_loosecut = tbsw.Processor(name="AlignTF_LC",proctype="FastTracker")
  trackfinder_loosecut.param("InputHitCollectionNameVec","hit_m26 hit_fei4 hit_tj")
  trackfinder_loosecut.param("ExcludeDetector", "")
  # HitQualitySelection==1: use cog hits and goehits for tracking
  # HitQualitySelection==0: only use goehits for tracking
  trackfinder_loosecut.param("HitQualitySelection", 1)
  trackfinder_loosecut.param("MaxTrackChi2", 10000000)
  trackfinder_loosecut.param("MaximumGap", 1)
  trackfinder_loosecut.param("MinimumHits",8)
  trackfinder_loosecut.param("OutlierChi2Cut", 100000000)
  trackfinder_loosecut.param("ParticleCharge","-1")
  trackfinder_loosecut.param("ParticleMass","0.000511")
  trackfinder_loosecut.param("ParticleMomentum", energy)
  trackfinder_loosecut.param("SingleHitSeeding", "0")
  trackfinder_loosecut.param("MaxResidualU","0.5")
  trackfinder_loosecut.param("MaxResidualV","0.5")
  prealigner_path.add_processor(trackfinder_loosecut)
 
  prealigner = tbsw.Processor(name="PreAligner",proctype="KalmanAligner")
  prealigner.param('ErrorsShiftX' , '0 10 10 10 10 10 0 10')
  prealigner.param('ErrorsShiftY' , '0 10 10 10 10 10 0 10')
  prealigner.param('ErrorsShiftZ' , '0 0 0 0 0 0 0 0')
  prealigner.param('ErrorsAlpha'  , '0 0 0 0 0 0 0 0')
  prealigner.param('ErrorsBeta'   , '0 0 0 0 0 0 0 0')
  prealigner.param('ErrorsGamma'  , '0 0.01 0.01 0.01 0.01 0.01 0 0.01')
  prealigner_path.add_processor(prealigner)  

  # Finished with path for prealigner
  calpaths.append(prealigner_path)  
  
  # Create path for alignment with tight cut track sample 
  aligner_path = Env.create_path('aligner_path')
  aligner_path.set_globals(params={'GearXMLFile': gearfile , 'MaxRecordNumber' : maxRecordNrShort, 'LCIOInputFiles': "tmp.slcio" })
  aligner_path.add_processor(geo)
  aligner_path = add_hitmakers(aligner_path)
  
  trackfinder_tightcut = tbsw.Processor(name="AlignTF_TC",proctype="FastTracker")
  trackfinder_tightcut.param("InputHitCollectionNameVec","hit_m26 hit_fei4 hit_tj")
  trackfinder_tightcut.param("ExcludeDetector", "")
  # HitQualitySelection==1: use cog hits and goehits for tracking
  # HitQualitySelection==0: only use goehits for tracking
  trackfinder_tightcut.param("HitQualitySelection", 1)
  trackfinder_tightcut.param("MaxTrackChi2", 100)
  trackfinder_tightcut.param("MaximumGap", 1)
  trackfinder_tightcut.param("MinimumHits",8)
  trackfinder_tightcut.param("OutlierChi2Cut", 20)
  trackfinder_tightcut.param("ParticleCharge","-1")
  trackfinder_tightcut.param("ParticleMass","0.000511")
  trackfinder_tightcut.param("ParticleMomentum", energy)
  trackfinder_tightcut.param("SingleHitSeeding", "0")
  trackfinder_tightcut.param("MaxResidualU","0.4")
  trackfinder_tightcut.param("MaxResidualV","0.4")
  aligner_path.add_processor(trackfinder_tightcut)
   
  aligner = tbsw.Processor(name="Aligner",proctype="KalmanAligner")
  aligner.param('ErrorsShiftX' , '0 10 10 10 10 10 0 10' )
  aligner.param('ErrorsShiftY' , '0 10 10 10 10 10 0 10')
  aligner.param('ErrorsShiftZ' , '0 10 10 10 10 10 0 10')
  aligner.param('ErrorsAlpha'  , '0 0 0 0.01 0 0 0 0')
  aligner.param('ErrorsBeta'   , '0 0 0 0.01 0 0 0 0')
  aligner.param('ErrorsGamma'  , '0 0.01 0.01 0.01 0.01 0.01 0 0.01')
  aligner_path.add_processor(aligner)    
  
  # Finished with path for aligner
  # Repeat this 3x
  calpaths.append(aligner_path)  
  calpaths.append(aligner_path)
  calpaths.append(aligner_path)
   
  # Creeate path for some track based dqm using current calibrations
  dqm_path = Env.create_path('dqm_path')
  dqm_path.set_globals(params={'GearXMLFile': gearfile , 'MaxRecordNumber' : maxRecordNrShort, 'LCIOInputFiles': "tmp.slcio" })
  dqm_path.add_processor(geo)
  dqm_path = add_hitmakers(dqm_path)
  dqm_path.add_processor(trackfinder_tightcut)

  teldqm = tbsw.Processor(name="TelescopeDQM", proctype="TrackFitDQM") 
  teldqm.param("RootFileName","TelescopeDQM.root")
  dqm_path.add_processor(teldqm)  
  
  # Finished with path for teldqm
  calpaths.append(dqm_path)
  
  if useClusterDB: 
    # The code below produces cluster calibration constants
    # (clusterDB). IF you only want to use CoG hits, this part
    # is not needed.
    
    # Creeate path for first iteration for computing clusterDBs for all sensors 
    preclustercal_path = Env.create_path('preclustercal_path')
    preclustercal_path.set_globals(params={'GearXMLFile': gearfile , 'MaxRecordNumber' : maxRecordNrLong, 'LCIOInputFiles': "tmp.slcio" })
    preclustercal_path.add_processor(geo)
    preclustercal_path = add_hitmakers(preclustercal_path) 
    preclustercal_path.add_processor(trackfinder_tightcut)      
    preclustercal_path = add_clustercalibrators(preclustercal_path)
    
    # Finished with path for pre cluster calibration 
    calpaths.append(preclustercal_path)
    
    # Create path for alignment with tight cut track sample and cluster DB
    aligner_db_path = Env.create_path('aligner_db_path')
    aligner_db_path.set_globals(params={'GearXMLFile': gearfile , 'MaxRecordNumber' : maxRecordNrShort, 'LCIOInputFiles': "tmp.slcio" })
    aligner_db_path.add_processor(geo)
    aligner_db_path = add_hitmakersDB(aligner_db_path) 
    aligner_db_path.add_processor(trackfinder_tightcut) 
    aligner_db_path.add_processor(aligner)   
    
    # Finished with path for alignemnt with hits from pre clusterDB 
    # Repeat this 2x
    for i in range(2):
      calpaths.append(aligner_db_path) 
    
    # Creeate path for next iterations for computing clusterDBs for all sensors 
    clustercal_path = Env.create_path('clustercal_path')
    clustercal_path.set_globals(params={'GearXMLFile': gearfile , 'MaxRecordNumber' : maxRecordNrLong, 'LCIOInputFiles': "tmp.slcio" })
    clustercal_path.add_processor(geo)
    clustercal_path = add_hitmakersDB(clustercal_path) 
    clustercal_path.add_processor(trackfinder_tightcut) 
    clustercal_path = add_clustercalibrators(clustercal_path)
     
    # Finished with path for pre cluster calibration
    # Repeat this 6x
    for i in range(6): 
      calpaths.append(clustercal_path)
             
    # Finished with path for alignemnt with hits from final clusterDB 
    # Repeat this 2x
    for i in range(2):
      calpaths.append(aligner_db_path) 
    
    # Creeate path for dqm using cluster calibrations
    dqm_db_path = Env.create_path('dqm_db_path')
    dqm_db_path.set_globals(params={'GearXMLFile': gearfile , 'MaxRecordNumber' : maxRecordNrShort, 'LCIOInputFiles': "tmp.slcio" })
    dqm_db_path.add_processor(geo)
    dqm_db_path = add_hitmakersDB(dqm_db_path)   
    dqm_db_path.add_processor(trackfinder_tightcut)  
    
    teldqm_db = tbsw.Processor(name="TelescopeDQM_DB", proctype="TrackFitDQM") 
    teldqm_db.param("RootFileName","TelescopeDQM_DB.root")
    dqm_db_path.add_processor(teldqm_db)  
    
    # Finished with path for dqm with cluster calibration
    calpaths.append(dqm_db_path)
    
  return calpaths


def create_reco_path(Env, rawfile, gearfile, energy, useClusterDB, runno):
  """
  Returns a list of tbsw path objects for reconstruciton of a test beam run 
  """
   
  reco_path = Env.create_path('reco_path')
  reco_path.set_globals(params={'GearXMLFile': gearfile , 'MaxRecordNumber' : maxRecordNrLongReco })
   
  rawinput = tbsw.Processor(name="RawInputProcessor",proctype="AsciiInputProcessor")
  rawinput.param("RawHitCollectionName", "zsdata")
  rawinput.param("RunNumber", runno)
  rawinput.param('FileNames', rawfile)
  reco_path.add_processor(rawinput)
  
  geo = tbsw.Processor(name="Geo",proctype="Geometry")
  geo.param("AlignmentDBFilePath", "localDB/alignmentDB.root")
  geo.param("ApplyAlignment", "true")
  geo.param("OverrideAlignment", "true")
  reco_path.add_processor(geo)  
  
  # Create path for all reconstruction up to hits
  reco_path = add_unpackers(reco_path)    
  reco_path = add_clusterizers(reco_path)    
   
  if useClusterDB: 
    reco_path = add_hitmakersDB(reco_path)   
  else: 
    reco_path = add_hitmakers(reco_path) 

  trackfinder = tbsw.Processor(name="TrackFinder",proctype="FastTracker")
  trackfinder.param("InputHitCollectionNameVec","hit_m26 hit_fei4")
  trackfinder.param("ExcludeDetector", "3")
  # HitQualitySelection==1: use cog hits and goehits for tracking
  # HitQualitySelection==0: only use goehits for tracking
  trackfinder.param("HitQualitySelection", 1) 
  trackfinder.param("MaxTrackChi2", "100")
  trackfinder.param("MaximumGap", "1")
  trackfinder.param("MinimumHits","6")
  trackfinder.param("OutlierChi2Cut", "20")
  trackfinder.param("ParticleCharge","-1")
  trackfinder.param("ParticleMass","0.000511")
  trackfinder.param("ParticleMomentum", energy)
  trackfinder.param("SingleHitSeeding", "0")
  trackfinder.param("MaxResidualU","0.4")
  trackfinder.param("MaxResidualV","0.4")
  reco_path.add_processor(trackfinder)  


  tj_analyzer = tbsw.Processor(name="TJAnalyzer",proctype="PixelDUTAnalyzer")
  tj_analyzer.param("HitCollection","hit_tj")  
  tj_analyzer.param("DigitCollection","zsdata_tj")
  tj_analyzer.param("DUTPlane","3")
  tj_analyzer.param("ReferencePlane","7")
  tj_analyzer.param("MaxResidualU","0.2")
  tj_analyzer.param("MaxResidualV","0.2")
  tj_analyzer.param("RootFileName","Histos-TJ.root")
  reco_path.add_processor(tj_analyzer)   
  
  return [ reco_path ]  
  
  
def calibrate(params,profile):
  
  rawfile, steerfiles, gearfile, energy, caltag, useClusterDB, runno = params
   
  # Calibrate of the run using beam data. Creates a folder cal-files/caltag 
  # containing all calibration data. 
  CalObj = tbsw.Calibration(steerfiles=steerfiles, name=caltag + '-cal')
  CalObj.profile = profile
  # Create list of calibration paths
  calpaths = create_calibration_path(CalObj, rawfile, gearfile, energy, useClusterDB, runno)
  # Run the calibration steps 
  CalObj.calibrate(paths=calpaths,ifile=rawfile,caltag=caltag)
   
  
def reconstruct(params,profile):
  
  rawfile, steerfiles, gearfile, energy, caltag, useClusterDB, runno = params 
   
  # Reconsruct the rawfile using caltag. Resulting root files are 
  # written to folder root-files/
  RecObj = tbsw.Reconstruction(steerfiles=steerfiles, name=caltag + '-reco' )
  RecObj.profile = profile
  # Create reconstuction path
  recopath = create_reco_path(RecObj, rawfile, gearfile, energy, useClusterDB, runno)  
  
  # Run the reconstuction  
  RecObj.reconstruct(paths=recopath,ifile=rawfile,caltag=caltag) 

if __name__ == '__main__':
  
  import argparse


  def str2bool(v):
    if v.lower() in ('yes', 'true', 'on','t', 'y', '1'):
      return True
    elif v.lower() in ('no', 'false', 'off','f', 'n', '0'):
      return False
    else:
      raise argparse.ArgumentTypeError('Boolean value expected.')

  parser = argparse.ArgumentParser(description="Perform calibration and reconstruction of a test beam run")
  parser.add_argument('--folder', dest='folder', default='angular_scan_TJMonopix/run53', type=str, help='Full path to folder with interpreted data')
  parser.add_argument('--runno', dest='runno', default=53, type=int, help='Run number')
  parser.add_argument('--steerfiles', dest='steerfiles', default='steering-files/tjmono-tb/', type=str, help='Path to steerfiles')
  parser.add_argument('--gearfile', dest='gearfile', default='gear_tjmono_60deg.xml', type=str, help='Name of gearfile inside steerfiles folder')
  parser.add_argument('--energy', dest='energy', default=5.2, type=float, help='Beam energy in GeV')
  parser.add_argument('--caltag', dest='caltag', default='', type=str, help='Name of calibration tag to use')
  parser.add_argument('--useClusterDB', dest='use_cluster_db', default=True, type=str2bool, help="Use cluster database")
  parser.add_argument('--skipCalibration', dest='skip_calibration', default=False, type=str2bool, help="Skip creating a new calibration tag")
  parser.add_argument('--skipReconstruction', dest='skip_reco', default=False, type=str2bool, help="Skip reconstruction of run")
  parser.add_argument('--profile', dest='profile', action='store_true', help="profile execution time")
  args = parser.parse_args()  
  
  import glob  
  
  # Find all .csv files in input folder
  rawfiles = glob.glob( args.folder + '/*.csv' )   
    
  # Sort the files to have m26 file at first position
  m26file = [rawfile for rawfile in rawfiles if "_pyBar_interpreted.csv" in rawfile ][0]
  rawfiles.insert(0, rawfiles.pop(rawfiles.index(m26file)))
  
  # Make sure that we have an absolute path
  rawfiles = " ".join([ os.path.abspath(rawfile) for rawfile in rawfiles ]) 
  
  print(rawfiles)
  
  if args.caltag=='':
      args.caltag = 'run' + str(args.runno) + '_' + os.path.splitext(  args.gearfile  )[0] 
    
  if not args.skip_calibration: 
      print("Creating new calibration tag {} from run {}".format(args.caltag, rawfiles))
      calibrate((rawfiles, args.steerfiles, args.gearfile, args.energy, args.caltag, args.use_cluster_db, args.runno),args.profile)
  
  if not args.skip_reco: 
      print("Reconstruct run {} using caltag {}".format(rawfiles, args.caltag))
      reconstruct((rawfiles, args.steerfiles, args.gearfile, args.energy, args.caltag, args.use_cluster_db, args.runno),args.profile)
  
