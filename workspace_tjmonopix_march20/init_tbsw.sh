#!/bin/bash
#tbsw commit SHA-1: 21cc8043ca73c301e51aa99cb203a646f28948f7 
#--------------------------------------------------------------------------------
#    ROOT                                                                        
#--------------------------------------------------------------------------------
export ROOTSYS=/home/benjamin/work/root-v6-10-08
export PATH=/home/benjamin/work/root-v6-10-08/bin:$PATH
export LD_LIBRARY_PATH=/home/benjamin/work/root-v6-10-08/lib:$LD_LIBRARY_PATH
export PYTHONPATH=/home/benjamin/work/root-v6-10-08/lib:/home/benjamin/Desktop/TBSW/tbsw/source:$PYTHONPATH
export ROOT_INCLUDE_PATH=/home/benjamin/Desktop/TBSW/tbsw/build

#--------------------------------------------------------------------------------
#    TBSW                                                                        
#--------------------------------------------------------------------------------
export PATH=/home/benjamin/Desktop/TBSW/tbsw/build/bin:$PATH
export LD_LIBRARY_PATH=/home/benjamin/Desktop/TBSW/tbsw/build/lib:$LD_LIBRARY_PATH
export MARLIN_DLL=/home/benjamin/Desktop/TBSW/tbsw/build/lib/libTBReco.so:/home/benjamin/Desktop/TBSW/tbsw/build/lib/libEudaqInput.so:
export MARLIN=/home/benjamin/Desktop/TBSW/tbsw/build

