#!/usr/bin/env python
# coding: utf8
"""
Script for submitting all jobs for calibration and reconstruction for tjmonopix testbeam 
March 2020 at Desy. 

Usage: python submit_reco_jobs.py

Author: Benjamin Schwenker <benjamin.schwenker@phys.uni-goettingen.de>  
"""

import multiprocessing
import subprocess



tasks = [ 
          "python tj-reco.py --folder=angular_scan_TJMonopix/run46 --gearfile=gear_tjmono_60deg.xml --runno=46",
          "python tj-reco.py --folder=angular_scan_TJMonopix/run52 --gearfile=gear_tjmono_60deg.xml --runno=52",
          #"python tj-reco.py --folder=angular_scan_TJMonopix/run53 --gearfile=gear_tjmono_60deg.xml --runno=53",
          #"python tj-reco.py --folder=angular_scan_TJMonopix/run54 --gearfile=gear_tjmono_50deg.xml --runno=54",
          "python tj-reco.py --folder=angular_scan_TJMonopix/run55 --gearfile=gear_tjmono_50deg.xml --runno=55",
          #"python tj-reco.py --folder=angular_scan_TJMonopix/run67 --gearfile=gear_tjmono_40deg.xml --runno=67",
          "python tj-reco.py --folder=angular_scan_TJMonopix/run69 --gearfile=gear_tjmono_40deg.xml --runno=69", 
          #"python tj-reco.py --folder=angular_scan_TJMonopix/run70 --gearfile=gear_tjmono_30deg.xml --runno=70",
          "python tj-reco.py --folder=angular_scan_TJMonopix/run71 --gearfile=gear_tjmono_30deg.xml --runno=71",
          #"python tj-reco.py --folder=angular_scan_TJMonopix/run72 --gearfile=gear_tjmono_20deg.xml --runno=72",
          "python tj-reco.py --folder=angular_scan_TJMonopix/run75 --gearfile=gear_tjmono_20deg.xml --runno=75",
          #"python tj-reco.py --folder=angular_scan_TJMonopix/run76 --gearfile=gear_tjmono_10deg.xml --runno=76",
          "python tj-reco.py --folder=angular_scan_TJMonopix/run79 --gearfile=gear_tjmono_10deg.xml --runno=79",
          #"python tj-reco.py --folder=angular_scan_TJMonopix/run80 --gearfile=gear_tjmono_0deg.xml --runno=80",
          "python tj-reco.py --folder=angular_scan_TJMonopix/run82 --gearfile=gear_tjmono_0deg.xml --runno=82",
        ]



def work(action):
    """
    Multiprocessing work
    
    Parameters
    ----------
    action : str
            command to be executed in shell to do work
    """
    print ('[INFO] ' + action + ' started ...')        
    retval=subprocess.call(action, shell=True)
    if retval < 0:
      raise RuntimeError("Process %s killed by signal %d "%(xmlfile, -retval))
    elif retval > 0:
      raise RuntimeError("Process %s failed with exit code %d "%(xmlfile, retval))
    print ('[INFO] ' + action + ' is done')          

    
if __name__ == "__main__":  
    print("There are %d CPUs on this machine" % multiprocessing.cpu_count())
    number_processes = 3
    pool = multiprocessing.Pool(number_processes)
    results = pool.map_async(work, tasks)
    pool.close()
    pool.join()

 
