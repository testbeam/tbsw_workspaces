# TJ Monopix 1 testbeam March 2020 

This workspace deals with the analysis of the TJ Monopix 1 conducted by Silab-Bonn in 
March 2020 at DESY. Responsible persons Toko Hirono, Tomasz Hemperek and Christian 
Bevin from University Bonn. The scripts/tools for processing the test beam data by Benjamin
Schwenker at University Goettingen.

For preparation of data: Install SiLab-Bonn/pyBAR_mimosa26_interpreter from GitHub (branch fixes or develop)

For calibration, reconstruction and analysis install tbsw from bitbucket https://bitbucket.org/testbeam/tbsw/src/master/


# Steps for analysis  

- convert pymosa raw data: 

```
python submit_converter_jobs.py
```

-prepare csv dumps for tbsw: 

```
python submit_csv_dumper_jobs.py
```

- calibrationa and reconstruction of data

```
python submit_reco_jobs.py
```

- making plots and histos 

```
python histo-plotter.py
```

Please have a look into the scripts for more details. 

Have fun, 

benjamin.schwenker@phys.uni-goettingen.de



 



