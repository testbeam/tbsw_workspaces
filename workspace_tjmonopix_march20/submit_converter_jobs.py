#!/usr/bin/env python
# coding: utf8
"""
Script for submitting all jobs for preparing csv dumps for tjmonopix testbeam 
March 2020 at Desy. 

Usage: python submit_converter_jobs.py

Author: Benjamin Schwenker <benjamin.schwenker@phys.uni-goettingen.de>  
"""

import multiprocessing
import subprocess

tasks = [ 
          "python pyBar-m26-converter.py -r=angular_scan_TJMonopix/run46/pymosa_46_200313-072734.h5",
          "python pyBar-m26-converter.py -r=angular_scan_TJMonopix/run52/pymosa_52_200313-095020.h5",
          #"python pyBar-m26-converter.py -r=angular_scan_TJMonopix/run53/pymosa_53_200313-105218.h5",
          #"python pyBar-m26-converter.py -r=angular_scan_TJMonopix/run54/pymosa_54_200313-112806.h5",
          "python pyBar-m26-converter.py -r=angular_scan_TJMonopix/run55/pymosa_55_200313-113435.h5",
          #"python pyBar-m26-converter.py -r=angular_scan_TJMonopix/run67/pymosa_67_200313-171142.h5", 
          "python pyBar-m26-converter.py -r=angular_scan_TJMonopix/run69/pymosa_69_200313-171650.h5", 
          #"python pyBar-m26-converter.py -r=angular_scan_TJMonopix/run70/pymosa_70_200313-190541.h5",
          "python pyBar-m26-converter.py -r=angular_scan_TJMonopix/run71/pymosa_71_200313-191056.h5",
          #"python pyBar-m26-converter.py -r=angular_scan_TJMonopix/run72/pymosa_72_200313-204734.h5",
          "python pyBar-m26-converter.py -r=angular_scan_TJMonopix/run75/pymosa_75_200313-212055.h5", 
          #"python pyBar-m26-converter.py -r=angular_scan_TJMonopix/run76/pymosa_76_200313-225606.h5",
          "python pyBar-m26-converter.py -r=angular_scan_TJMonopix/run79/pymosa_79_200314-105028.h5", 
          #"python pyBar-m26-converter.py -r=angular_scan_TJMonopix/run80/pymosa_80_200314-122651.h5",
          "python pyBar-m26-converter.py -r=angular_scan_TJMonopix/run82/pymosa_82_200314-123454.h5",
        ]


def work(action):
    """
    Multiprocessing work
    
    Parameters
    ----------
    action : str
            command to be executed in shell to do work
    """
    print ('[INFO] ' + action + ' started ...')        
    retval=subprocess.call(action, shell=True)
    if retval < 0:
      raise RuntimeError("Process %s killed by signal %d "%(xmlfile, -retval))
    elif retval > 0:
      raise RuntimeError("Process %s failed with exit code %d "%(xmlfile, retval))
    print ('[INFO] ' + action + ' is done')          

    
if __name__ == "__main__":  
    print("There are %d CPUs on this machine" % multiprocessing.cpu_count())
    number_processes = 3
    pool = multiprocessing.Pool(number_processes)
    results = pool.map_async(work, tasks)
    pool.close()
    pool.join()

 
