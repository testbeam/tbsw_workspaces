"""
This is an example script to demonstrate how TBSW can be used to create an X0 image from a 
test beam experiment.



The script has four main steps: 

1) Telescope calibration: Hot pixel masking, telescope alignment and cluster calibration (only from air run)
2) Angle reconstruction: Reconstruction of kink angles on the scattering object (from all runs: air, Al, DUT)   
3) X0 calibration: Obtain X0 calibration constants using X0 calibraiton runs (all runs with known scatterer) 
4) X0 imaging: Obtain calibrated X0 images of unknown scattering objects   

Usage: 

python x0-reco.py --startStep=1 --stopStep=4 --folder=angular_scan_TJMonopix/run80 --runno=80 --gearfile=gear_tjmono_0deg.xml

python x0-reco.py --startStep=2 --stopStep=4 --folder=angular_scan_TJMonopix/run82 --runno=82 --gearfile=gear_tjmono_0deg.xml --caltag=run82_gear_tjmono_0deg

Have fun with X0 imaging. 


Author: Ulf Stolzenberg <ulf.stolzenberg@phys.uni-goettingen.de>  
Author: Benjamin Schwenker <benjamin.schwenker@phys.uni-goettingen.de>  
"""

import tbsw 
import os
import multiprocessing
import argparse
import glob

# Script purpose option: Determines which steps should be 
# processed by the script. All steps with associated integer values
# between the start and stop parameter are conducted. The following list 
# provides the integer values of the individual reconstruction steps:

# 1: Telescope calibration (and target alignment if enabled)
# 2: Angle reconstruction
# 3: X0 calibration
# 4: X0 imaging

# This default setting means that all reconstruction steps are performed
parser = argparse.ArgumentParser(description="Perform calibration and reconstruction of a test beam run")
parser.add_argument('--startStep', dest='startStep', default=0, type=int, help='Start processing at this step number. Steps are 1) Telescope calibration, 2) Angle reconstruction, 3) X0 calibration, 4) X0 imaging')
parser.add_argument('--stopStep', dest='stopStep', default=4, type=int, help='Stop processing at this step number. Steps are 1) Telescope calibration, 2) Angle reconstruction, 3) X0 calibration, 4) X0 imaging')
parser.add_argument('--folder', dest='folder', default='angular_scan_TJMonopix/run53', type=str, help='Full path to folder with interpreted data')
parser.add_argument('--runno', dest='runno', default=53, type=int, help='Run number')
parser.add_argument('--gearfile', dest='gearfile', default='x0_gear_tjmono_60deg.xml', type=str, help='Name of gearfile inside steerfiles folder')
parser.add_argument('--caltag', dest='caltag', default='', type=str, help='Name of calibration tag to use')
args = parser.parse_args()

# Path to steering files 
# Folder contains a gear file detailing the detector geometry and a config file
# for x0 calibration. Users will likely want to rename this folder. 
steerfiles = 'steering-files/tjmono-tb/'

# Nominal Beam energy
beamenergy=5.2

if args.caltag=='':
  args.caltag='x0_run' +str(args.runno) + '_' + os.path.splitext(  args.gearfile  )[0] 

# Find all .csv files in input folder
rawfiles = glob.glob( args.folder + '/*_pyBar_interpreted.csv' )
    
if len(rawfiles)==0: 
  print('Cannot find rawfiles')

# Make sure that we have an absolute path
rawfile = os.path.abspath(rawfiles[0])

# Determine cluster resolution and store in cluster DB?
Use_clusterDB=True

# By default,the track finder constructs track seeds using hit pairs from two 
# planes. With SingleHitSeeding, seed tracks are constructed from a single hit 
# and extrapolated parallel to the z axis. This can safe time but risks missing hits. 
Use_SingleHitSeeding=True

# Finding correlations between first and last sensor can be difficult
# for low momentum tracks and/or large distances between sensors.
# By default, a robust method is used that correlates sensors step by
# step. Only deactivate when you are really certain you do not need
# this feature (expert decision).  
Use_LongTelescopeCali=False

# Switch to use clusters on outer planes to calculate cluster resolution
# The track resolution is expected to be worse on the outer planes, using them may 
# have a negative impact on the determined cluster resolutions
UseOuterPlanesForClusterDB=False

# Flag to indicate that real EUTelescope data is used (raw format)
mcdata=False

# Input data is in csv format
csvdata=True


# File names and lists of filenames for the different steps 

# Eudaq raw files used during telescope calibration. Telescope calibration 
# includes the alignment of the reference telescope and the calibration
# of its spatial resolution. 
rawfile_cali = rawfile  



# List of runs, which are used as input for the scattering angle reconstruction
# The angle reconstruction step is essential and every run, that will be used later during the x0 calibration or x0 imaging steps, must be listed
RunList_reco = [
		        rawfile
               ]


RawfileList_reco = [x for x in RunList_reco]


# List of runs, which are input for the x0 calibration
# Typically runs with various different materials and thicknesses have to be used to achieve a sensible calibration.
# Good results can for example be achieved with air (no material between the telescope arms) and two different 
# aluminium thicknesses.
#
# In most cases two 1-2 runs with approximately 1 million events per thickness/material are sufficient for a good x0 calibration
#
# The different measurement regions and other options have to be set in the x0.cfg file in the steer files directory
RunList_x0cali = [
		    rawfile
          ]

RawfileList_x0cali = [x for x in RunList_x0cali]

# List of runs, which are input for the first x0 image
# Use only runs, with exactly the same target material and positioning
RunList_x0image = [
		    rawfile
          ]

# Set the name of this image
name_image1='tjmono'

RawfileList_x0image = [x for x in RunList_x0image]


# Number of events ...
# for telescope calibration
nevents_cali = 50000



# for angle reconstruction (-1 use all available events)
nevents_reco = -1   
  
if __name__ == '__main__':


  # Calibrate the telescope 
  # In case you already have all the DB files from another telescope calibration 
  # and want to reuse it, just switch to Script_purpose_option 0 or 1
  #
  # DQM plots like track p/chi2 values, residuals and other interesting parameters
  # from this telescope calibration step can be found as pdf files in 
  # workspace/results/telescopeDQM

  if args.startStep < 2 and args.stopStep >= 1:
    tbsw.x0script_functions.calibrate( rawfile_cali, steerfiles, args.caltag, args.gearfile, nevents_cali, Use_clusterDB, beamenergy, mcdata, Use_LongTelescopeCali, UseOuterPlanesForClusterDB, csvdata=True)

    

  # Angle reconstruction
  # In case you already have reconstructed the scattering angles for all
  # the runs you are interested in, just switch to Script_purpose_option 0 or 1
  #
  # The root files with the reconstructed angles and other parameters (see 
  # README_X0.md for a full list and some descriptions) can be found in 
  # workspace/root-files/X0-run*runnumber, etc*-reco.root
  # The histmap and angle resolution for every single run can be found in 
  # workspace/results/anglerecoDQM/
  if args.startStep < 3 and args.stopStep >= 2:
    params_reco=[(x, steerfiles, args.caltag, args.gearfile, nevents_reco, Use_SingleHitSeeding, Use_clusterDB, beamenergy, mcdata, csvdata) for x in RawfileList_reco]
    print "The parameters for the reconstruction are: " 
    print params_reco
    
    def work(params):
      """ Multiprocessing work
      """
      rawfile, steerfiles, caltag, gearfile, nevents, Use_SingleHitSeeding, Use_clusterDB, beamenergy, mcdata, csvdata = params
      tbsw.x0script_functions.reconstruct(rawfile, steerfiles, caltag, gearfile, nevents, Use_SingleHitSeeding, Use_clusterDB, beamenergy, mcdata, csvdata)
                  
    count = min(2,multiprocessing.cpu_count())
    pool = multiprocessing.Pool(processes=count)
    results = pool.map_async(work, params_reco)
    pool.close()
    pool.join()    
     
    for rawfile in RawfileList_reco:
      params=(rawfile, args.caltag)
      tbsw.x0script_functions.reconstruction_DQM(rawfile, args.caltag)

  # Start x0 calibration
  # In case you already have the x0 calibration DB file from a previous x0 calibration 
  # and want to reuse it, just switch to Script_purpose_option 0
  #
  # The fitted distributions and self-consistency plots in pdf format from this 
  # x0 calibration can be found in the workspace/tmp-runs/*X0Calibration/ directory
  if args.startStep < 4 and args.stopStep >= 3:
    tbsw.x0script_functions.xx0calibration(RawfileList_x0cali, steerfiles, args.caltag)

  # Generate a calibrated X/X0 image
  #
  # The calibrated radiation length image and other images, such as the beamspot
  # etc can be found in the workspace/root-files/*CalibratedX0Image.root
  if args.startStep < 5 and args.stopStep >= 4:
    tbsw.x0script_functions.xx0image(RawfileList_x0image, steerfiles, args.caltag, name_image1)

  
