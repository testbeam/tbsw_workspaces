# TJ Monopix 1 testbeam March 2020 

This workspace deals with the analysis of the TJ Monopix 1 conducted by Silab-Bonn in 
March 2020 at DESY. Responsible persons Toko Hirono, Tomasz Hemperek and Christian 
Bevin from University Bonn. The scripts/tools for processing the test beam data by Benjamin
Schwenker at University Goettingen.

A small subset of a telescope run with a TJ Monopix 1 DUT installed in the center of the EUDET 
telescope with 6 M26 sensors. A trigger chip with a FEI-4 readout was installed to provide timing hits.
The data from the three subdetectors is available as hit tables in csv format. The syncronization of
hits is based on the trigger number in the hit tables and will be done in tbsw. 

Usage: 
```
python reco.py --folder=rawdata --gearfile=gear_tjmono_50deg.xml --runno=54
```

```
python histo-plotter.py
```

Please have a look into the scripts for more details. 

Have fun, 

benjamin.schwenker@phys.uni-goettingen.de



 



