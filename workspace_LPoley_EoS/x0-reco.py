"""
This is an example script to demonstrate how TBSW can be used to create an X0 image from a 
test beam experiment.

The script assumes data was taken with the EUDET/AIDA tracking telescope in a monoenergetic beam.
In order to calibrate the tracking telescope, it is necessary to take data with no scattering object
placed in between the telescope arm. This situation is called an air run. The next step is to 
calibrate the X0 measurements by putting a series of Al plates with known thicknesses in between 
the telesocpe arms. After the x0 calibration data was taken, the scattering object to be studied can be 
placed in the telescope. Neither the beam energy nor the telescope planes should be touched in between 
these runs. 

The script has four main steps: 

1) Telescope calibration: Hot pixel masking, telescope alignment and cluster calibration (only from air run)
2) Angle reconstruction: Reconstruction of kink angles on the scattering object (from all runs: air, Al, DUT)   
3) X0 calibration: Obtain X0 calibration constants using X0 calibraiton runs (all runs with known scatterer) 
4) X0 imaging: Obtain calibrated X0 images of unknown scattering objects   

The script must be modified by most users to their specific use case. Places where such modifications 
are needed are commented in the text below. Examples for such modifactions are the pathes to the rawdata
files, settings for beam energy and the definition of objects used for the X0 calibration. 

Have fun with X0 imaging. 

Author: Ulf Stolzenberg <ulf.stolzenberg@phys.uni-goettingen.de>  
Author: Benjamin Schwenker <benjamin.schwenker@phys.uni-goettingen.de>  
"""

import tbsw 
import os
import multiprocessing
import argparse

# Script purpose option: Determines which steps should be 
# processed by the script. All steps with associated integer values
# between the start and stop parameter are conducted. The following list 
# provides the integer values of the individual reconstruction steps:

# 1: Telescope calibration (and target alignment if enabled)
# 2: Angle reconstruction
# 3: X0 calibration
# 4: X0 imaging

# This default setting means that all reconstruction steps are performed
parser = argparse.ArgumentParser(description="Perform calibration and reconstruction of a test beam run")
parser.add_argument('--startStep', dest='startStep', default=0, type=int, help='Start processing at this step number. Steps are 1) Telescope calibration, 2) Angle reconstruction, 3) X0 calibration, 4) X0 imaging')
parser.add_argument('--stopStep', dest='stopStep', default=4, type=int, help='Stop processing at this step number. Steps are 1) Telescope calibration, 2) Angle reconstruction, 3) X0 calibration, 4) X0 imaging')
args = parser.parse_args()

# Path to steering files 
# Folder contains a gear file detailing the detector geometry and a config file
# for x0 calibration. Users will likely want to rename this folder. 
steerfiles = 'steering-files/x0-tb-eos-epsilon0/'

# Nominal Beam energy
beamenergy=2.0

# Definition of the calibration tag. It is typically named after telescope setup, beam energy, x0calibration target etc.
# The caltag is used to generate a directory under localDB/*caltag* where all calibration parameters are stored
# in local DB files. Additionally DQM plots of the calibration steps will be stored under results/ to cross check the
# calibration results. The telescope calibration step (Step 1 in the enumeration above) will generate a hotpixel mask (NoiseDB-M26.root),
# a files with alignment information (alignmentDB.root) and a data base containing cluster resolutions (clusterDB-M26.root). DQM plots
# of the cluster calibration are stored under results/clusterDB-M26/*caltag*. Other track based DQM plots such as track p values,
# pulls and the mean number of tracks per event are stored for example in results/TelescopeDQM2/*caltag*.
# During the radiation length calibration step (Step 3) the beam energy, the beam energy gradients and a global offset of the telescope
# angle resolution will be determined and stored in a text file (x0cal_result.cfg). The DQM plots such as a selfconsistency diagram and
# angle distributions with their associated fits can be found in results/x0calibrationDQM/*caltag*.
caltag='tb-eos-2GeV-epsilon0'

# Name of the gearfile, which describes the telescope setup 
# Must be placed in the steerfiles folder
gearfile = 'gear_long_telescope.xml'

# Determine cluster resolution and store in cluster DB?
Use_clusterDB=True

# By default,the track finder constructs track seeds using hit pairs from two 
# planes. With SingleHitSeeding, seed tracks are constructed from a single hit 
# and extrapolated parallel to the z axis. This can safe time but risks missing hits. 
Use_SingleHitSeeding=False

# Finding correlations between first and last sensor can be difficult
# for low momentum tracks and/or large distances between sensors.
# By default, a robust method is used that correlates sensors step by
# step. Only deactivate when you are really certain you do not need
# this feature (expert decision).  
Use_LongTelescopeCali=True

# Switch to use clusters on outer planes to calculate cluster resolution
# The track resolution is expected to be worse on the outer planes, using them may 
# have a negative impact on the determined cluster resolutions
UseOuterPlanesForClusterDB=False

# Flag to indicate that real EUTelescope data is used (raw format)
mcdata=False

# By default, the z position of the X0 target is defined by an mechanical survey
# measurement. For sufficiently thick targets, we can correct the z position of 
# the X0 target by forming a vertex from the upstream and downstream track. 
# This option is deactivated by default. If you know what you are doing, you can 
# enable it by using a positive number of iterations (expert decision)  
targetalignment_iterations=0

# File names and lists of filenames for the different steps 

# global path to raw files
rawfile_path='/home/luise/TBSW/tbsw/workspace/data/'

# Eudaq raw files used during telescope calibration. Telescope calibration 
# includes the alignment of the reference telescope and the calibration
# of its spatial resolution. 
# Best use a run without a scattering target in between the telescope arms.
# The calibration has to be done for every telescope setup, beam energy and m26 threshold settings
cali_run='run000210.raw'
rawfile_cali = rawfile_path + cali_run

# raw file used for target alignment (only useful with a thick (X/X0 > 5 %) scattering target)
TA_run='run006958.raw'
rawfile_TA = rawfile_path + TA_run

# List of runs, which are used as input for the scattering angle reconstruction
# The angle reconstruction step is essential and every run, that will be used later during the x0 calibration or x0 imaging steps, must be listed
RunList_reco = [
		    'run000210.raw', #air
		    'run000154.raw', #4 mm Alu
		    'run000161.raw', #6 mm Alu
		    'run000140.raw', #0.5 mm Alu
		    'run000132.raw', #1.5 mm Alu
		    'run000147.raw', #3 mm Alu
		    'run000374.raw', #EoS1r
		    'run000375.raw', #EoS1r
		    'run000376.raw', #EoS1r
		    'run000377.raw', #EoS1r
		    'run000378.raw', #EoS1r
		    'run000379.raw', #EoS1r
          ]

RawfileList_reco = [rawfile_path+x for x in RunList_reco]

# List of runs, which are input for the x0 calibration
# Typically runs with various different materials and thicknesses have to be used to achieve a sensible calibration.
# Good results can for example be achieved with air (no material between the telescope arms) and two different 
# aluminium thicknesses.
#
# In most cases two 1-2 runs with approximately 1 million events per thickness/material are sufficient for a good x0 calibration
#
# The different measurement regions and other options have to be set in the x0.cfg file in the steer files directory
RunList_x0cali = [
		    'run000154.raw', #4 mm Alu
		    'run000210.raw', #air
		    'run000161.raw', #6 mm Alu
		    'run000140.raw', #0.5 mm Alu
		    'run000132.raw', #1.5 mm Alu
		    'run000147.raw', #3 mm Alu
          ]

RawfileList_x0cali = [rawfile_path+x for x in RunList_x0cali]

# List of runs, which are input for the first x0 image
# Use only runs, with exactly the same target material and positioning
RunList_x0image = [
		    #'run000287.raw', #EoS1a
		    #'run000288.raw', #EoS1a
		    #'run000289.raw', #EoS1a
		    #'run000290.raw', #EoS1a
		    #'run000291.raw', #EoS1a
		    #'run000292.raw', #EoS1b
		    #'run000293.raw', #EoS1b
		    #'run000294.raw', #EoS1b
		    #'run000295.raw', #EoS1b
		    #'run000296.raw', #EoS1b
		    #'run000297.raw', #EoS1c
		    #'run000298.raw', #EoS1c
		    #'run000299.raw', #EoS1c
		    #'run000300.raw', #EoS1c
		    #'run000301.raw', #EoS1c
		    #'run000302.raw', #EoS1d
		    #'run000303.raw', #EoS1d
		    #'run000304.raw', #EoS1d
		    #'run000305.raw', #EoS1d
		    #'run000306.raw', #EoS1d
		    #'run000307.raw', #EoS1e
		    #'run000308.raw', #EoS1e
		    #'run000309.raw', #EoS1e
		    #'run000310.raw', #EoS1e
		    #'run000311.raw', #EoS1e
		    #'run000312.raw', #EoS1f
		    #'run000313.raw', #EoS1f
		    #'run000314.raw', #EoS1f
		    #'run000315.raw', #EoS1f
		    #'run000316.raw', #EoS1f
		    #'run000317.raw', #EoS1g
		    #'run000318.raw', #EoS1g
		    #'run000319.raw', #EoS1g
		    #'run000320.raw', #EoS1g
		    #'run000321.raw', #EoS1g
		    #'run000322.raw', #EoS1h
		    #'run000323.raw', #EoS1h
		    #'run000324.raw', #EoS1h
		    #'run000325.raw', #EoS1h
		    #'run000326.raw', #EoS1h
		    #'run000327.raw', #EoS1i
		    #'run000328.raw', #EoS1i
		    #'run000329.raw', #EoS1i
		    #'run000330.raw', #EoS1i
		    #'run000331.raw', #EoS1i
		    #'run000332.raw', #EoS1j
		    #'run000333.raw', #EoS1j
		    #'run000334.raw', #EoS1j
		    #'run000335.raw', #EoS1j
		    #'run000336.raw', #EoS1j
		    #'run000337.raw', #EoS1k
		    #'run000338.raw', #EoS1k
		    #'run000339.raw', #EoS1k
		    #'run000340.raw', #EoS1k
		    #'run000341.raw', #EoS1k
		    #'run000342.raw', #EoS1l
		    #'run000343.raw', #EoS1l
		    #'run000344.raw', #EoS1l
		    #'run000345.raw', #EoS1l
		    #'run000346.raw', #EoS1l
		    #'run000347.raw', #EoS1m
		    #'run000348.raw', #EoS1m
		    #'run000349.raw', #EoS1m
		    #'run000350.raw', #EoS1m
		    #'run000351.raw', #EoS1m
		    #'run000353.raw', #EoS1n
		    #'run000354.raw', #EoS1n
		    #'run000355.raw', #EoS1n
		    #'run000356.raw', #EoS1n
		    #'run000357.raw', #EoS1n
		    #'run000358.raw', #EoS1o
		    #'run000359.raw', #EoS1o
		    #'run000360.raw', #EoS1o
		    #'run000361.raw', #EoS1o
		    #'run000362.raw', #EoS1o
		    #'run000363.raw', #EoS1p
		    #'run000364.raw', #EoS1p
		    #'run000365.raw', #EoS1p
		    #'run000366.raw', #EoS1p
		    #'run000367.raw', #EoS1p
		    #'run000368.raw', #EoS1q
		    #'run000369.raw', #EoS1q
		    #'run000370.raw', #EoS1q
		    #'run000371.raw', #EoS1q
		    #'run000372.raw', #EoS1q
		    #'run000373.raw', #EoS1q
		    'run000374.raw', #EoS1r
		    'run000375.raw', #EoS1r
		    'run000376.raw', #EoS1r
		    'run000377.raw', #EoS1r
		    'run000378.raw', #EoS1r
		    'run000379.raw', #EoS1r
		    #'run000447.raw', #EoS2a
		    #'run000448.raw', #EoS2a
		    #'run000449.raw', #EoS2a
		    #'run000450.raw', #EoS2a
		    #'run000451.raw', #EoS2a
		    #'run000452.raw', #EoS2a
		    #'run000453.raw', #EoS2b
		    #'run000454.raw', #EoS2b
		    #'run000455.raw', #EoS2b
		    #'run000456.raw', #EoS2b
		    #'run000457.raw', #EoS2b
		    #'run000458.raw', #EoS2b
		    #'run000459.raw', #EoS2c
		    #'run000460.raw', #EoS2c
		    #'run000461.raw', #EoS2c
		    #'run000462.raw', #EoS2c
		    #'run000463.raw', #EoS2c
		    #'run000464.raw', #EoS2c
		    #'run000465.raw', #EoS2d
		    #'run000466.raw', #EoS2d
		    #'run000467.raw', #EoS2d
		    #'run000468.raw', #EoS2d
		    #'run000469.raw', #EoS2d
		    #'run000470.raw', #EoS2d
		    #'run000472.raw', #EoS2e
		    #'run000473.raw', #EoS2e
		    #'run000474.raw', #EoS2e
		    #'run000475.raw', #EoS2e
		    #'run000476.raw', #EoS2e
		    #'run000477.raw', #EoS2e
		    #'run000478.raw', #EoS2f
		    #'run000479.raw', #EoS2f
		    #'run000480.raw', #EoS2f
		    #'run000481.raw', #EoS2f
		    #'run000482.raw', #EoS2f
		    #'run000483.raw', #EoS2f
		    #'run000484.raw', #EoS2g
		    #'run000485.raw', #EoS2g
		    #'run000486.raw', #EoS2g
		    #'run000487.raw', #EoS2g
		    #'run000488.raw', #EoS2g
		    #'run000489.raw', #EoS2g
		    #'run000490.raw', #EoS2g
		    #'run000491.raw', #EoS2h
		    #'run000492.raw', #EoS2h
		    #'run000493.raw', #EoS2h
		    #'run000494.raw', #EoS2h
		    #'run000495.raw', #EoS2h
		    #'run000496.raw', #EoS2i
		    #'run000497.raw', #EoS2i
		    #'run000498.raw', #EoS2i
		    #'run000499.raw', #EoS2i
		    #'run000500.raw', #EoS2i
		    #'run000501.raw', #EoS2i
		    #'run000502.raw', #EoS2j
		    #'run000503.raw', #EoS2j
		    #'run000504.raw', #EoS2j
		    #'run000505.raw', #EoS2j
		    #'run000506.raw', #EoS2j
		    #'run000507.raw', #EoS2j
		    #'run000508.raw', #EoS2k
		    #'run000509.raw', #EoS2k
		    #'run000510.raw', #EoS2k
		    #'run000511.raw', #EoS2k
		    #'run000512.raw', #EoS2k
		    #'run000513.raw', #EoS2k
		    #'run000514.raw', #EoS2l
		    #'run000515.raw', #EoS2l
		    #'run000516.raw', #EoS2l
		    #'run000517.raw', #EoS2l
		    #'run000518.raw', #EoS2l
		    #'run000519.raw', #EoS2l
		    #'run000520.raw', #EoS2l
		    #'run000521.raw', #EoS2m
		    #'run000522.raw', #EoS2m
		    #'run000523.raw', #EoS2m
		    #'run000524.raw', #EoS2m
		    #'run000525.raw', #EoS2m
		    #'run000526.raw', #EoS2m
		    #'run000527.raw', #EoS2n
		    #'run000528.raw', #EoS2n
		    #'run000529.raw', #EoS2n
		    #'run000530.raw', #EoS2n
		    #'run000531.raw', #EoS2n
		    #'run000532.raw', #EoS2n
          ]

# Set the name of this image
name_image1='6mm-alu'

RawfileList_x0image = [rawfile_path+x for x in RunList_x0image]


# List of runs, which are input for the second x0 image
# Remove comment in case you want to produce more than one image
#RunList_x0image2 = [
		    #'run000154.raw', #other material
         # ]

# Set the name of this image
#name_image2='4mm-alu'

#RawfileList_x0image2 = [rawfile_path+x for x in RunList_x0image2]

# Number of events ...
# for telescope calibration
nevents_cali = 50000

# for target alignment
nevents_TA = 1000000

# for angle reconstruction (-1 use all available events)
nevents_reco = -1   
  
if __name__ == '__main__':


  # Calibrate the telescope 
  # In case you already have all the DB files from another telescope calibration 
  # and want to reuse it, just switch to Script_purpose_option 0 or 1
  #
  # DQM plots like track p/chi2 values, residuals and other interesting parameters
  # from this telescope calibration step can be found as pdf files in 
  # workspace/results/telescopeDQM

  if args.startStep < 2 and args.stopStep >= 1:
    tbsw.x0script_functions.calibrate( rawfile_cali, steerfiles, caltag, gearfile, nevents_cali, Use_clusterDB, beamenergy, mcdata, Use_LongTelescopeCali, UseOuterPlanesForClusterDB)

    # Target alignment
    for it in range(0,targetalignment_iterations):
      tbsw.x0script_functions.targetalignment(rawfile_TA, steerfiles, it, caltag, gearfile, nevents_TA, Use_clusterDB, beamenergy, mcdata)

  # Angle reconstruction
  # In case you already have reconstructed the scattering angles for all
  # the runs you are interested in, just switch to Script_purpose_option 0 or 1
  #
  # The root files with the reconstructed angles and other parameters (see 
  # README_X0.md for a full list and some descriptions) can be found in 
  # workspace/root-files/X0-run*runnumber, etc*-reco.root
  # The histmap and angle resolution for every single run can be found in 
  # workspace/results/anglerecoDQM/
  if args.startStep < 3 and args.stopStep >= 2:
    params_reco=[(x, steerfiles, caltag, gearfile, nevents_reco, Use_SingleHitSeeding, Use_clusterDB, beamenergy, mcdata) for x in RawfileList_reco]
    print "The parameters for the reconstruction are: " 
    print params_reco

    count = multiprocessing.cpu_count()
    pool = multiprocessing.Pool(processes=count)
    pool.map(tbsw.x0script_functions.reconstruct, params_reco)

    for rawfile in RawfileList_reco:
      params=(rawfile, caltag)
      tbsw.x0script_functions.reconstruction_DQM(rawfile, caltag)

  # Start x0 calibration
  # In case you already have the x0 calibration DB file from a previous x0 calibration 
  # and want to reuse it, just switch to Script_purpose_option 0
  #
  # The fitted distributions and self-consistency plots in pdf format from this 
  # x0 calibration can be found in the workspace/tmp-runs/*X0Calibration/ directory
  if args.startStep < 4 and args.stopStep >= 3:
    tbsw.x0script_functions.xx0calibration(RawfileList_x0cali, steerfiles, caltag)

  # Generate a calibrated X/X0 image
  #
  # The calibrated radiation length image and other images, such as the beamspot
  # etc can be found in the workspace/root-files/*CalibratedX0Image.root
  if args.startStep < 5 and args.stopStep >= 4:
    tbsw.x0script_functions.xx0image(RawfileList_x0image, steerfiles, caltag, name_image1)

  # Generate another calibrated X/X0 image
  # The X/X0 image step can be repeated multiple times to generate a set of images
  # Just remove the comment and add a run list for each image
    tbsw.x0script_functions.xx0image(RawfileList_x0image2, steerfiles, caltag, name_image2)

