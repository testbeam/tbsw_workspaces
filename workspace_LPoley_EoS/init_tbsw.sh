#!/bin/bash
#tbsw commit SHA-1: ed2961e3cf17382dcd46faab0a88eb3891530e39 
#--------------------------------------------------------------------------------
#    ROOT                                                                        
#--------------------------------------------------------------------------------
export ROOTSYS=/home/luise/TBSW/ROOT_build
export PATH=/home/luise/TBSW/ROOT_build/bin:$PATH
export LD_LIBRARY_PATH=/home/luise/TBSW/ROOT_build/lib:$LD_LIBRARY_PATH
export PYTHONPATH=/home/luise/TBSW/ROOT_build/lib:/home/luise/TBSW/tbsw/source:$PYTHONPATH
export ROOT_INCLUDE_PATH=/home/luise/TBSW/tbsw/build

#--------------------------------------------------------------------------------
#    TBSW                                                                        
#--------------------------------------------------------------------------------
export PATH=/home/luise/TBSW/tbsw/build/bin:$PATH
export LD_LIBRARY_PATH=/home/luise/TBSW/tbsw/build/lib:$LD_LIBRARY_PATH
export MARLIN_DLL=/home/luise/TBSW/tbsw/build/lib/libTBReco.so:/home/luise/TBSW/tbsw/build/lib/libEudaqInput.so:
export MARLIN=/home/luise/TBSW/tbsw/build

