"""
This is an example script to demonstrate how TBSW can be used to create an X0 image from a 
test beam experiment.



The script has four main steps: 

1) Telescope calibration: Hot pixel masking, telescope alignment and cluster calibration (only from air run)
2) Angle reconstruction: Reconstruction of kink angles on the scattering object (from all runs: air, Al, DUT)   
3) X0 calibration: Obtain X0 calibration constants using X0 calibraiton runs (all runs with known scatterer) 
4) X0 imaging: Obtain calibrated X0 images of unknown scattering objects   


The .raw files from this X0 test beam can be downloaded. First create a directory (for example at 
'$HOME/rawdata/example') where they are stored. Afterwards download the files:

wget -O $HOME/rawdata/example/run003367.raw  https://owncloud.gwdg.de/index.php/s/oyiIHXyrte4SRT1/download
wget -O $HOME/rawdata/example/run003368.raw  https://owncloud.gwdg.de/index.php/s/o6ktUb1kL6GL4D0/download
wget -O $HOME/rawdata/example/run003374.raw  https://owncloud.gwdg.de/index.php/s/61kZelC9BGTzi4W/download
wget -O $HOME/rawdata/example/run003375.raw  https://owncloud.gwdg.de/index.php/s/xFjd7aUFEWKX3Te/download
wget -O $HOME/rawdata/example/run003387.raw  https://owncloud.gwdg.de/index.php/s/3l7Q9f0h2Etkf6r/download
wget -O $HOME/rawdata/example/run003388.raw  https://owncloud.gwdg.de/index.php/s/6ZQQ1YhmgBdiShM/download
wget -O $HOME/rawdata/example/run003394.raw  https://owncloud.gwdg.de/index.php/s/lIVmuJhHJwSR1Gj/download
wget -O $HOME/rawdata/example/run003395.raw  https://owncloud.gwdg.de/index.php/s/uwMyqgZHp1swOO4/download
      
Take into account that the total size of these files is ~3 GB and the download may take a while.

A set of reference results for the data processing can be found in the folder reference. This is meant to 
provide a means to check for changes in the tbsw code. 

Usage: 

python x0-reco-tbjuly17.py --startStep 1 --stopStep 4

Have fun with X0 imaging. 


Author: Ulf Stolzenberg <ulf.stolzenberg@phys.uni-goettingen.de>  
Author: Benjamin Schwenker <benjamin.schwenker@phys.uni-goettingen.de>  
"""

import tbsw 
import os
import multiprocessing
import argparse

# Script purpose option: Determines which steps should be 
# processed by the script. All steps with associated integer values
# between the start and stop parameter are conducted. The following list 
# provides the integer values of the individual reconstruction steps:

# 1: Telescope calibration (and target alignment if enabled)
# 2: Angle reconstruction
# 3: X0 calibration
# 4: X0 imaging

# This default setting means that all reconstruction steps are performed
parser = argparse.ArgumentParser(description="Perform calibration and reconstruction of a test beam run")
parser.add_argument('--startStep', dest='startStep', default=0, type=int, help='Start processing at this step number. Steps are 1) Telescope calibration, 2) Angle reconstruction, 3) X0 calibration, 4) X0 imaging')
parser.add_argument('--stopStep', dest='stopStep', default=4, type=int, help='Stop processing at this step number. Steps are 1) Telescope calibration, 2) Angle reconstruction, 3) X0 calibration, 4) X0 imaging')
args = parser.parse_args()

# Path to steering files 
# Folder contains a gear file detailing the detector geometry and a config file
# for x0 calibration. Users will likely want to rename this folder. 
steerfiles = 'steering-files/x0-tb-3GeV-geo1/'

# Nominal Beam energy
beamenergy=3.0

# Definition of the calibration tag. It is typically named after telescope setup, beam energy, x0calibration target etc.
# The caltag is used to generate a directory under localDB/*caltag* where all calibration parameters are stored
# in local DB files. Additionally DQM plots of the calibration steps will be stored under results/ to cross check the
# calibration results. The telescope calibration step (Step 1 in the enumeration above) will generate a hotpixel mask (NoiseDB-M26.root),
# a files with alignment information (alignmentDB.root) and a data base containing cluster resolutions (clusterDB-M26.root). DQM plots
# of the cluster calibration are stored under results/clusterDB-M26/*caltag*. Other track based DQM plots such as track p values,
# pulls and the mean number of tracks per event are stored for example in results/TelescopeDQM2/*caltag*.
# During the radiation length calibration step (Step 3) the beam energy, the beam energy gradients and a global offset of the telescope
# angle resolution will be determined and stored in a text file (x0cal_result.cfg). The DQM plots such as a selfconsistency diagram and
# angle distributions with their associated fits can be found in results/x0calibrationDQM/*caltag*.
caltag='tb2017-geo1-3GeV'

# Name of the gearfile, which describes the telescope setup 
# Must be placed in the steerfiles folder
gearfile = 'gear.xml'

# Determine cluster resolution and store in cluster DB?
Use_clusterDB=True

# By default,the track finder constructs track seeds using hit pairs from two 
# planes. With SingleHitSeeding, seed tracks are constructed from a single hit 
# and extrapolated parallel to the z axis. This can safe time but risks missing hits. 
Use_SingleHitSeeding=True

# Finding correlations between first and last sensor can be difficult
# for low momentum tracks and/or large distances between sensors.
# By default, a robust method is used that correlates sensors step by
# step. Only deactivate when you are really certain you do not need
# this feature (expert decision).  
Use_LongTelescopeCali=True

# Switch to use clusters on outer planes to calculate cluster resolution
# The track resolution is expected to be worse on the outer planes, using them may 
# have a negative impact on the determined cluster resolutions
UseOuterPlanesForClusterDB=False

# Flag to indicate that real EUTelescope data is used (raw format)
mcdata=False

# By default, the z position of the X0 target is defined by an mechanical survey
# measurement. For sufficiently thick targets, we can correct the z position of 
# the X0 target by forming a vertex from the upstream and downstream track. 
# This option is deactivated by default. If you know what you are doing, you can 
# enable it by using a positive number of iterations (expert decision)  
targetalignment_iterations=0

# File names and lists of filenames for the different steps 

# global path to raw files
rawfile_path='/work1/rawdata/tbjune17/'

# Eudaq raw files used during telescope calibration. Telescope calibration 
# includes the alignment of the reference telescope and the calibration
# of its spatial resolution. 
# Best use a run without a scattering target in between the telescope arms.
# The calibration has to be done for every telescope setup, beam energy and m26 threshold settings
cali_run='run003395.raw'
rawfile_cali = rawfile_path + cali_run

# raw file used for target alignment (only useful with a thick (X/X0 > 5 %) scattering target)
TA_run='run006958.raw'
rawfile_TA = rawfile_path + TA_run

# List of runs, which are used as input for the scattering angle reconstruction
# The angle reconstruction step is essential and every run, that will be used later during the x0 calibration or x0 imaging steps, must be listed
RunList_reco = [
		    'run003394.raw', #air
		    'run003395.raw', #air
		    'run003387.raw', #0.5 mm Alu
		    'run003388.raw', #0.5 mm Alu
		    'run003374.raw', #1.0 mm Alu
		    'run003375.raw', #1.0 mm Alu
		    'run003367.raw', #1.5 mm Alu
		    'run003368.raw', #1.5 mm Alu
          ]

RawfileList_reco = [rawfile_path+x for x in RunList_reco]

# List of runs, which are input for the x0 calibration
# Typically runs with various different materials and thicknesses have to be used to achieve a sensible calibration.
# Good results can for example be achieved with air (no material between the telescope arms) and two different 
# aluminium thicknesses.
#
# In most cases two 1-2 runs with approximately 1 million events per thickness/material are sufficient for a good x0 calibration
#
# The different measurement regions and other options have to be set in the x0.cfg file in the steer files directory
RunList_x0cali = [
		    'run003394.raw', #air
		    'run003395.raw', #air
		    'run003387.raw', #0.5 mm Alu
		    'run003388.raw', #0.5 mm Alu
		    'run003374.raw', #1.0 mm Alu
		    'run003375.raw', #1.0 mm Alu
		    'run003367.raw', #1.5 mm Alu
		    'run003368.raw', #1.5 mm Alu
          ]

RawfileList_x0cali = [rawfile_path+x for x in RunList_x0cali]

# List of runs, which are input for the first x0 image
# Use only runs, with exactly the same target material and positioning
RunList_x0image = [
		    'run003394.raw', #air
		    'run003395.raw', #air
          ]

# Set the name of this image
name_image1='air'

RawfileList_x0image = [rawfile_path+x for x in RunList_x0image]


# List of runs, which are input for the second x0 image
# Remove comment in case you want to produce more than one image
RunList_x0image2 = [
		    'run003387.raw', #0.5 mm Alu
		    'run003388.raw', #0.5 mm Alu
          ]

# Set the name of this image
name_image2='0p5mmalu'

RawfileList_x0image2 = [rawfile_path+x for x in RunList_x0image2]


RunList_x0image3 = [
		    'run003374.raw', #1.0 mm Alu
		    'run003375.raw', #1.0 mm Alu
          ]

# Set the name of this image
name_image3='1mmalu'

RawfileList_x0image3 = [rawfile_path+x for x in RunList_x0image3]

# Number of events ...
# for telescope calibration
nevents_cali = 50000

# for target alignment
nevents_TA = 1000000

# for angle reconstruction (-1 use all available events)
nevents_reco = -1   
  
if __name__ == '__main__':


  # Calibrate the telescope 
  # In case you already have all the DB files from another telescope calibration 
  # and want to reuse it, just switch to Script_purpose_option 0 or 1
  #
  # DQM plots like track p/chi2 values, residuals and other interesting parameters
  # from this telescope calibration step can be found as pdf files in 
  # workspace/results/telescopeDQM

  if args.startStep < 2 and args.stopStep >= 1:
    tbsw.x0script_functions.calibrate( rawfile_cali, steerfiles, caltag, gearfile, nevents_cali, Use_clusterDB, beamenergy, mcdata, Use_LongTelescopeCali, UseOuterPlanesForClusterDB)

    # Target alignment
    for it in range(0,targetalignment_iterations):
      tbsw.x0script_functions.targetalignment(rawfile_TA, steerfiles, it, caltag, gearfile, nevents_TA, Use_clusterDB, beamenergy, mcdata)

  # Angle reconstruction
  # In case you already have reconstructed the scattering angles for all
  # the runs you are interested in, just switch to Script_purpose_option 0 or 1
  #
  # The root files with the reconstructed angles and other parameters (see 
  # README_X0.md for a full list and some descriptions) can be found in 
  # workspace/root-files/X0-run*runnumber, etc*-reco.root
  # The histmap and angle resolution for every single run can be found in 
  # workspace/results/anglerecoDQM/
  if args.startStep < 3 and args.stopStep >= 2:
    params_reco=[(x, steerfiles, caltag, gearfile, nevents_reco, Use_SingleHitSeeding, Use_clusterDB, beamenergy, mcdata) for x in RawfileList_reco]
    print "The parameters for the reconstruction are: " 
    print params_reco

    count = multiprocessing.cpu_count()
    pool = multiprocessing.Pool(processes=count)
    pool.map(tbsw.x0script_functions.reconstruct, params_reco)

    for rawfile in RawfileList_reco:
      params=(rawfile, caltag)
      tbsw.x0script_functions.reconstruction_DQM(rawfile, caltag)

  # Start x0 calibration
  # In case you already have the x0 calibration DB file from a previous x0 calibration 
  # and want to reuse it, just switch to Script_purpose_option 0
  #
  # The fitted distributions and self-consistency plots in pdf format from this 
  # x0 calibration can be found in the workspace/tmp-runs/*X0Calibration/ directory
  if args.startStep < 4 and args.stopStep >= 3:
    tbsw.x0script_functions.xx0calibration(RawfileList_x0cali, steerfiles, caltag)

  # Generate a calibrated X/X0 image
  #
  # The calibrated radiation length image and other images, such as the beamspot
  # etc can be found in the workspace/root-files/*CalibratedX0Image.root
  if args.startStep < 5 and args.stopStep >= 4:
    tbsw.x0script_functions.xx0image(RawfileList_x0image, steerfiles, caltag, name_image1)

  # Generate another calibrated X/X0 image
  # The X/X0 image step can be repeated multiple times to generate a set of images
  # Just remove the comment and add a run list for each image
    tbsw.x0script_functions.xx0image(RawfileList_x0image2, steerfiles, caltag, name_image2)

    tbsw.x0script_functions.xx0image(RawfileList_x0image3, steerfiles, caltag, name_image3)

